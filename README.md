# BenchmarkNotebooks


Notebooks for testing and benchmarking DSP using numpy, pythran, cython, numba
and Julia. Published on my [website](https://jochenschroeder.com/blog).
